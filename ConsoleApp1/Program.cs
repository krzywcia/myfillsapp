﻿using System;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			int num1 = 0;
			int num2;
			Console.WriteLine("Calculator\r");
			Console.WriteLine("---------------------------------\t");

			Console.WriteLine("Type a number, and then press Enter");
			num1 = Convert.ToInt32(Console.ReadLine());

			Console.WriteLine("Type another a number, and then press Enter");
			num2 = Convert.ToInt32(Console.ReadLine());

			Console.WriteLine("Choose an option from the following list:\r");
			Console.WriteLine("\ta - Add");
			Console.WriteLine("\ts - Subtract");
			Console.WriteLine("\tm - Multiply");
			Console.WriteLine("\td - Divide");
			Console.Write("Your option? ");

			switch (Console.ReadLine())
			{
				case "a":
					Console.WriteLine($"Your result: {num1} + {num2} = " + (num1 + num2));
					break;

				case "s":
					Console.WriteLine($"Your result: {num1} - {num2} = " + (num1 - num2));
					break;

				case "m":
					Console.WriteLine($"Your result: {num1} * {num2} = " + (num1 * num2));
					break;

				case "d":
					Console.WriteLine($"Your result: {num1} / {num2} = " + (num1 / num2));
					break;
			}

			Console.Write("Press any key to close the Calculator console app...");
			Console.ReadKey();

		}
	}
}
